package gui;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import root.Kontroller;

public class Start extends Application {
	
	private Kontroller control;

	private TextArea statustext;
	private Stage fenster;
	private Button[] buttonfeld;

	@Override
	public void start(Stage stage) throws Exception {
		control = new Kontroller(this);
		fenster = stage;
		initKomponenten();
		fenster.setScene(new Scene(erzeugeLayout()));
		fenster.sizeToScene();
		fenster.setTitle("Kryptomat");
		fenster.setResizable(false);
		fenster.show();
	}
	
	private Parent erzeugeLayout(){
		BorderPane border = new BorderPane();
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(5,10,5,10));
		Pane[] gridfeld = new Pane[8];
		gridfeld[0] = new Pane();
		gridfeld[0].setPadding(new Insets(3,3,3,3));
		gridfeld[1] = new Pane();
		gridfeld[1].setPadding(new Insets(3,3,3,3));
		gridfeld[2] = new Pane();
		gridfeld[2].setPadding(new Insets(3,3,3,3));
		gridfeld[3] = new Pane();
		gridfeld[3].setPadding(new Insets(3,3,3,3));
		gridfeld[4] = new Pane();
		gridfeld[4].setPadding(new Insets(3,3,3,3));
		gridfeld[5] = new Pane();
		gridfeld[5].setPadding(new Insets(3,3,3,3));
		gridfeld[6] = new Pane();
		gridfeld[6].setPadding(new Insets(3,3,3,3));
		gridfeld[7] = new Pane();
		gridfeld[7].setPadding(new Insets(3,3,3,3));
		
		gridfeld[0].getChildren().add(buttonfeld[0]);
		gridfeld[1].getChildren().add(buttonfeld[1]);
		gridfeld[2].getChildren().add(buttonfeld[2]);
		gridfeld[3].getChildren().add(buttonfeld[3]);
		gridfeld[4].getChildren().add(buttonfeld[4]);
		gridfeld[5].getChildren().add(buttonfeld[5]);
		gridfeld[6].getChildren().add(buttonfeld[6]);
		gridfeld[7].getChildren().add(buttonfeld[7]);
		grid.add(gridfeld[0], 0, 0);
		grid.add(gridfeld[1], 0, 1);
		grid.add(gridfeld[2], 0, 2);
		grid.add(gridfeld[3], 0, 3);
		grid.add(gridfeld[4], 1, 0);
		grid.add(gridfeld[5], 1, 1);
		grid.add(gridfeld[6], 1, 2);
		grid.add(gridfeld[7], 1, 3);
		
		statustext = new TextArea();
		statustext.setEditable(false);
		statustext.setWrapText(true);
		ScrollPane scroll = new ScrollPane(statustext);
		
		border.setTop(scroll);
		border.setCenter(grid);
		return border;
	}
	
	private void initKomponenten(){
		buttonfeld = new Button[8];
		for(int n = 0; n < buttonfeld.length; n++){
			buttonfeld[n] = new Button();
			buttonfeld[n].setMinSize(225, 25);
			buttonfeld[n].setOnAction(new Handler());
		}
		
		buttonfeld[0].setText("Schluesselpaar generieren");
		buttonfeld[1].setText("Schluesselpaar speichern");
		buttonfeld[2].setText("Daten verschluesseln");
		buttonfeld[3].setText("Pruefsumme einer Datei erzeugen");
		buttonfeld[4].setText("Schluesselpaar Ueberpruefen");
		buttonfeld[5].setText("Schluesselpaar laden");
		buttonfeld[6].setText("Daten entschluesseln");
		buttonfeld[7].setText("Pruefsummen vergleichen");
	}

	private void erzeugeSchluessel(){
		control.erzeugeSchluessel();
	}
	
	private void schluesselpaarSpeichern(){
		control.speicherPaar();
	}

	private void dateiVerschluesseln(){
		control.dateiVerschluesseln();
	}
	
	private void hashErzeugen(){
		control.erzeugeHash();
	}
	
	private void paarPruefen(){
		control.paarPruefen();
	}
	
	private void schluesselpaarLaden(){
		control.ladePaar();
	}

	private void dateiEntschluesseln(){
		control.dateiEntschluesseln();
	}
	
	private void hashPruefen(){
		control.pruefeHashwerte();
	}	

	public void setStatustext(String status){
		statustext.appendText(status+"\n");
	}
	
	private class Handler implements EventHandler<ActionEvent> {
		
		@Override
		public void handle(ActionEvent action) {
			
			if ( action.getSource() == buttonfeld[0]){
				erzeugeSchluessel();
			} else if ( action.getSource() == buttonfeld[1]){
				schluesselpaarSpeichern();
			} else if ( action.getSource() == buttonfeld[2]){
				dateiVerschluesseln();
			} else if ( action.getSource() == buttonfeld[3]){
				hashErzeugen();
			} else if ( action.getSource() == buttonfeld[4]){
				paarPruefen();
			} else if ( action.getSource() == buttonfeld[5]){
				schluesselpaarLaden();
			} else if ( action.getSource() == buttonfeld[6]){
				dateiEntschluesseln();
			} else if ( action.getSource() == buttonfeld[7]){
				hashPruefen();
			}
		}
	}

	public static void main(String[] args){
		launch(args);
	}

}
