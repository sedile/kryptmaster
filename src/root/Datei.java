package root;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Datei {
	
	private File datei;
	private String path;
	private String endung;
	
	Datei(File datei, boolean speichern ){
		this.datei = datei;
		if ( speichern ){
			endung = getExtension(datei);
		} else {
			String temp = datei.getAbsolutePath();
			int dotA = temp.indexOf(".");
			int dotB = temp.indexOf(".", dotA+1);
			endung = temp.substring(dotA, dotB);
			path = temp.replaceAll("\\.(\\S+)", "");
		}
	}
	
	private String getExtension(File datei){
		String extension = new String();
		Pattern regex = Pattern.compile("\\.(\\S+)");
		Matcher match = regex.matcher(datei.getAbsolutePath());
		if ( match.find() ){
			extension += match.group(1);
			return extension;
		} else {
			return "";
		}
	}

	File getFile(){
		return datei;
	}
	
	String getPathLoad(){
		return path;
	}

	String getExtension(){
		return endung;
	}

}
