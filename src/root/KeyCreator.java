package root;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

class KeyCreator {
	
	/**
	 * generiert ein RSA-Schluesselpaar mit 4096-bit Schluessellaenge
	 * @return Schluesselpaar
	 */
	static KeyPair generateKeyPair(){
		KeyPairGenerator keygen;
		try {
			keygen = KeyPairGenerator.getInstance("RSA");
			keygen.initialize(4096);
			KeyPair key = keygen.generateKeyPair();
			return key;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * prueft, ob ein Schluesselpaar zusammengehoert
	 * @param publickey zu testender publickey
	 * @param privatekey zu testender privatekey
	 * @return true, schluesselpaar gehoert zusammen, sonst false
	 * @throws Exception Fehler
	 */
	static boolean pruefePaar(PublicKey publickey, PrivateKey privatekey) throws Exception {
		String test = "TEST";
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publickey);
		byte[] chiffrat = cipher.doFinal(test.getBytes());
		Cipher cipher2 = Cipher.getInstance("RSA");
		cipher2.init(Cipher.DECRYPT_MODE, privatekey);
		byte[] dec = cipher2.doFinal(chiffrat);
		String erg = new String(dec);
		if ( test.equals(erg) ){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * generiert ein Sitzungsschluessel (AES)
	 * @return der Sitzungsschluessel
	 */
	static SecretKey generateKey(){
		KeyGenerator keygen;
		try {
			keygen = KeyGenerator.getInstance("AES");
			keygen.init(new SecureRandom());
			SecretKey sesson = keygen.generateKey();
			return sesson;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

}
