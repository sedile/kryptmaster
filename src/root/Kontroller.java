package root;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import gui.Start;

public class Kontroller {
	
	private Start startfenster;
	private PublicKey publickey;
	private PrivateKey privatekey;

	public Kontroller(Start fenster){
		startfenster = fenster;
		publickey = null;
		privatekey = null;
	}

	public boolean erzeugeSchluessel(){
		KeyPair keypair = KeyCreator.generateKeyPair();
		if ( keypair != null ){
			setPublicKey(keypair.getPublic());
			setPrivateKey(keypair.getPrivate());
			setStatustext("------------------------------------------------------------------------------------------------");
			setStatustext("Schluesselpaar wurde erfolreich generiert.");
			setStatustext("[HINWEIS] : Der Private-Key sollte NIEMALS zusammen mit den Public-Key auf ein Medium gespeichert werden. Das Schluesselpaar gut aufbewahren!");
			setStatustext("Mit den Public-Key wird eine Datei verschluesselt und mit dem zugehoerigen Private-Key wird die Datei wieder entschluesselt.");
			setStatustext("------------------------------------------------------------------------------------------------");
			return true;
		} else {
			setStatustext("[FEHLER] : Generieren eines Schluesselpaars fehlgeschlagen");
			return false;
		}
	}
	
	public void speicherPaar(){
		Manager manager = new Manager(this);
		if ( publickey != null & privatekey != null ){
			if ( manager.savePublicKey() & manager.savePrivateKey() ){
				setStatustext("[INFO] : Public-Key wurde in '"+manager.getDatapath()+"' gespeichert");
				setStatustext("[INFO] : Private-Key wurde in < "+manager.getDatapath()+" > gespeichert.");
			} else {
				setStatustext("[FEHLER] : Schluesselpaar wurde nicht gespeichert.");
				setPublicKey(null);
				setPrivateKey(null);
			}
		} else {
			setStatustext("[FEHLER] : Schluesselpaar wurde nicht gespeichert.");
			setPublicKey(null);
			setPrivateKey(null);
		}
	}

	public void dateiVerschluesseln(){
		if ( publickey != null ){
			Manager manager = new Manager(this);
			manager.readPlainFiles();
		} else {
			setStatustext("[HINWEIS] : Bitte zuerst ein Schluesselpaar generieren oder ein bereits vorhandenen Schluesselpaar laden zum verschluesseln einer Datei.");
		}
	}
	
	public void erzeugeHash(){
		if ( Manager.createHashFromFile() ){
			setStatustext("[HINWEIS] : Pruefsumme zu der Ausgewaehlten Datei wurde gespeichert.");
		} else {
			setStatustext("[FEHLER] : Fehler beim ermitteln der Pruefsumme.");
		}
	}
	
	public void pruefeHashwerte(){
		if ( Manager.getHashFromFile() ){
			setStatustext("[HINWEIS] : Pruefsumme der Datei stimmt ueberein.");
		} else {
			setStatustext("[WARNUNG] : Pruefsummen stimmen nicht ueberein. Entweder es wurde eine falsche Datei ausgewaehlt oder die Datei wurde manipuliert.");
		}
	}
	
	public void paarPruefen(){
		boolean erg = false;
		if ( publickey != null && privatekey != null ){
			try {
				erg = KeyCreator.pruefePaar(publickey, privatekey);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if ( erg ){
			setStatustext("[HINWEIS] : Das Schluesselpaar gehoert zusammen.");
		} else {
			setStatustext("[FEHLER] : Das Schluesselpaar gehoert nicht zusammen, da entweder der Public-Key oder der Private-Key nicht zueinander passen.");
		}
	}
	
	public void ladePaar(){
		Manager manager = new Manager(this);
		if ( manager.loadPublicKey() & manager.loadPrivateKey() ){
			setStatustext("[INFO] : Public-Key wurde aus < "+manager.getDatapath()+" > geladen.");
			setStatustext("[HINWEIS] : Private-Key wurde aus < "+manager.getDatapath()+" > geladen.");
			setPublicKey(manager.getPublicKey());
			setPrivateKey(manager.getPrivateKey());
		} else {
			setStatustext("[FEHLER] : Schluesselpaar bzw ein Teilschluessel wurde nicht geladen.");
			setPublicKey(null);
			setPrivateKey(null);
		}
	}

	public void dateiEntschluesseln(){
		if ( privatekey != null ){
			Manager manager = new Manager(this);
			manager.readCryptFiles();
		} else {
			setStatustext("[HINWEIS] : Bitte zuerst ein Schluesselpaar laden, bevor eine Datei entschluesselt werden kann.");
		}
	}

	public void setPublicKey(PublicKey publickey){
		this.publickey = publickey;
	}
	
	public void setPrivateKey(PrivateKey privatekey){
		this.privatekey = privatekey;
	}

	public PublicKey getPublicKey(){
		return publickey;
	}
	
	public PrivateKey getPrivateKey(){
		return privatekey;
	}

	boolean[] getStatus(){
		boolean[] stat = new boolean[2];
		if ( publickey == null ){
			stat[0] = false;
		} else {
			stat[0] = true;
		}
		
		if ( privatekey == null ){
			stat[1] = false;
		} else {
			stat[1] = true;
		}
		return stat;
	}

	void setStatustext(String status){
		startfenster.setStatustext(status);
	}
}
