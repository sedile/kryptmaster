package root;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

class Manager {
	
	private Kontroller control;
	private PublicKey publickey;
	private PrivateKey privatekey;
	private String datapath;
	private ArrayList<Datei> dateiliste;
	
	Manager(Kontroller control){
		this.control = control;
		dateiliste = new ArrayList<Datei>();
	}
	
	boolean savePublicKey(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Public-Key speichern (puk)");
		fc.getExtensionFilters().add(new ExtensionFilter("Public-Key Datei", "*.puk"));	
		File publickeyfile = fc.showSaveDialog(null);
		if ( publickeyfile != null ){
			datapath = publickeyfile.getAbsolutePath();		
			try {
				ObjectOutputStream pubout = new ObjectOutputStream(new FileOutputStream(publickeyfile));
				pubout.writeObject(control.getPublicKey());
				pubout.close();
				return true;
			} catch (IOException e) {
				control.setStatustext("Public-Key wurde nicht gespeichert");
				e.printStackTrace();
				return false;
			}
		} else {
			control.setStatustext("Speichervorgang abgebrochen");
			return false;
		}
	}

	boolean savePrivateKey(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Private-Key speichern (prk)");
		fc.getExtensionFilters().add(new ExtensionFilter("Private-Key Datei","*.prk"));
		File privatekeyfile = fc.showSaveDialog(null);
		if ( privatekeyfile != null ){
			datapath = privatekeyfile.getAbsolutePath();
			try {
				ObjectOutputStream priout = new ObjectOutputStream(new FileOutputStream(privatekeyfile));
				priout.writeObject(control.getPrivateKey());
				priout.close();
				return true;
			} catch (IOException e) {
				control.setStatustext("Private-Key wurde nicht gespeichert");
				e.printStackTrace();
				return false;
			}
		} else {
			control.setStatustext("Speichervorgang abgebrochen");
			return false;
		}
	}
	
	boolean loadPublicKey(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Public-Key laden");
		File publickeyfile = fc.showOpenDialog(null);
		if ( publickeyfile != null ){
			datapath = publickeyfile.getAbsolutePath();
			try {
				ObjectInputStream pubin = new ObjectInputStream(new FileInputStream(publickeyfile));	
				publickey = (PublicKey) pubin.readObject();
				control.setPublicKey(publickey);		
				control.setStatustext("Public-Key wurde erfolgreich eingelesen.");
				pubin.close();
				return true;
			} catch (IOException | ClassNotFoundException e) {
				control.setStatustext("Public-Key wurde nicht eingelesen");
				e.printStackTrace();
				return false;
			}
		} else {
			control.setStatustext("Ladevorgang abgebrochen");
			return false;
		}
	}
	
	boolean loadPrivateKey(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Private-Key laden");
		File privatekeyfile = fc.showOpenDialog(null);
		if ( privatekeyfile != null ){
			datapath = privatekeyfile.getAbsolutePath();	
			try {
				ObjectInputStream priin = new ObjectInputStream(new FileInputStream(privatekeyfile));	
				privatekey = (PrivateKey) priin.readObject();
				control.setPrivateKey(privatekey);	
				control.setStatustext("Private-Key wurde erfolgreich eingelesen.");
				priin.close();
				return true;
			} catch (IOException | ClassNotFoundException e) {
				control.setStatustext("Private-Key Datei enthaelt ein Fehler");
				e.printStackTrace();
				return false;
			}
		} else {
			control.setStatustext("Ladevorgang abgebrochen");
			return false;
		}
	}
	
	static boolean getHashFromFile(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Datei auswaehlen um Pruefsumme zu pruefen");
		File datei = fc.showOpenDialog(null);
		if ( datei != null ){
			byte[] buffer = new byte[8192];
			int read = 0;
			
			try {
				InputStream input = new FileInputStream(datei);
				MessageDigest sha = MessageDigest.getInstance("SHA-512");
				
				while( ( read = input.read(buffer)) > 0 ){
					sha.update(buffer, 0, read);
				}
				
				input.close();
				byte[] hashwert = sha.digest();
				String hash = DatatypeConverter.printHexBinary(hashwert).toLowerCase();
				if ( getHashFromFile(hash) ){
					return true;
				} else {
					return false;
				}
			} catch (IOException | NoSuchAlgorithmException e) {
				e.printStackTrace();
				return false;
			}		
		}
		return false;
	}
	
	private static boolean getHashFromFile(String hashNew){
		if ( hashNew.isEmpty() ){
			return false;
		}
		
		String text = new String();
		String hashOld = new String();
		FileChooser fc = new FileChooser();
		fc.setTitle("Pruefsumme der zu pruefenden Datei laden");
		File hashAlt = fc.showOpenDialog(null);
		if ( hashAlt != null ){
			try {
				BufferedReader input = new BufferedReader(new FileReader(hashAlt));
				while( (text = input.readLine()) != null ){
					hashOld += text;
				}
				input.close();
				
				if ( hashOld.equals(hashNew) ){
					return true;
				} else {
					return false;
				}	
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}
	
	static boolean createHashFromFile(){
		FileChooser fc = new FileChooser();
		fc.setTitle("Datei auswaehlen");
		File datei = fc.showOpenDialog(null);
		if ( datei != null ){
			byte[] buffer = new byte[8192];
			int read = 0;
			
			try {
				InputStream input = new FileInputStream(datei);
				MessageDigest sha = MessageDigest.getInstance("SHA-512");
				
				while( ( read = input.read(buffer)) > 0 ){
					sha.update(buffer, 0, read);
				}
				
				input.close();
				byte[] hashwert = sha.digest();
				String hash = DatatypeConverter.printHexBinary(hashwert).toLowerCase();
				if (saveHashFromFile(hash) ){
					return true;
				} else {
					return false;
				}
			} catch (IOException | NoSuchAlgorithmException e) {
				e.printStackTrace();
				return false;
			}		
		}
		return false;
	}
	
	private static boolean saveHashFromFile(String hash){
		if ( hash.isEmpty() ){
			return false;
		}
		
		FileChooser fc = new FileChooser();
		fc.setTitle("Speicher Pruefsumme der Datei");
		fc.getExtensionFilters().add(new ExtensionFilter("Text-Datei","*.txt"));
		File hashfile = fc.showSaveDialog(null);
		if ( hashfile != null ){
			try {
				FileWriter out = new FileWriter(hashfile);
				out.write(hash);
				out.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	void readPlainFiles() {
		FileChooser fc = new FileChooser();
		fc.setTitle("Zu verschluesselnde Datei(en) auswaehlen");
		List<File> files = fc.showOpenMultipleDialog(null);
		if ( files != null ){
			for(int n = 0; n < files.size(); n++){
				dateiliste.add(new Datei(files.get(n), true));
				encryptFile(control.getPublicKey(), dateiliste.get(n));
				control.setStatustext("Verschluesselte Datei wurde in '"+dateiliste.get(n).getFile().getAbsolutePath()+"' gespeichert");
			}
		}
		dateiliste.clear();
	}
	
	void readCryptFiles() {
		FileChooser fc = new FileChooser();
		fc.setTitle("Zu entschluesselnde Datei(en) auswaehlen");
		List<File> files = fc.showOpenMultipleDialog(null);
		if ( files != null ){
			for(int n = 0; n < files.size(); n++){
				dateiliste.add(new Datei(files.get(n), false));
				decryptFile(control.getPrivateKey(), dateiliste.get(n));
				control.setStatustext("Entschluesselte Datei wurde in '"+dateiliste.get(n).getPathLoad()+"' gespeichert");	
			}
		}
		dateiliste.clear();
	}
	
	private OutputStream encryptOutputStream(OutputStream os, PublicKey publickey){
		Key sesson = KeyCreator.generateKey();
		
		try {
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.WRAP_MODE, publickey);
			byte[] wrappedKey = cipher.wrap(sesson);
			os.write(wrappedKey);
			
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, sesson);
			return new CipherOutputStream(os, cipher);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	void encryptFile(PublicKey publickey, Datei datei) {
		FileChooser fc = new FileChooser();
		fc.setTitle("Verschluesselte Datei speichern");
		fc.getExtensionFilters().add(new ExtensionFilter("Alle Dateien","*.*"));
		File destfile = new File(datei.getFile()+"."+datei.getExtension());
		
		try {
			InputStream is = new FileInputStream(datei.getFile());
			OutputStream out = encryptOutputStream(new FileOutputStream(destfile), publickey);
			byte[] buffer = new byte[128];
			int length;
				
			while ((length = is.read(buffer)) != -1 ){
				out.write(buffer,0,length);
			}
				
			out.flush();
			out.close();
			is.close();
		} catch (IOException e) {
			control.setStatustext("Datei wurde nicht gespeichert");
			e.printStackTrace();
		}
	}
	
	private void decryptFile(PrivateKey privatekey, Datei datei) {
		FileChooser fc = new FileChooser();
		fc.setTitle("entschluesselte Datei speichern");
		fc.getExtensionFilters().add(new ExtensionFilter("Alle Dateien","*.*"));
		File destfile = new File(datei.getPathLoad()+""+datei.getExtension());
		
		try {
			InputStream is = decryptInputStream(new FileInputStream(datei.getFile()), privatekey);
			OutputStream out = new FileOutputStream(destfile);
			byte[] buffer = new byte[128];
			int length;
						
			while ((length = is.read(buffer)) != -1 ){
				out.write(buffer,0,length);
			}
						
			out.flush();
			out.close();
			is.close();
		} catch (IOException e) {
			control.setStatustext("Datei wurde nicht gespeichert");
			e.printStackTrace();
		}
	}

	private InputStream decryptInputStream(InputStream is, PrivateKey privatekey){
		byte[] wrappedKey = new byte[512];
		try {
			Cipher cipher = Cipher.getInstance("RSA");
			is.read(wrappedKey, 0 , 512);
			
			cipher.init(Cipher.UNWRAP_MODE, privatekey);
			Key sesson = cipher.unwrap(wrappedKey, "AES", Cipher.SECRET_KEY); 
			
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, sesson);
			return new CipherInputStream(is, cipher);
			
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | IOException | InvalidKeyException e) {
			control.setStatustext("Schluesselpaar gehoert nicht zusammen!");
			e.printStackTrace();
		}
		return null;
	}

	String getDatapath(){
		return datapath;
	}

	PublicKey getPublicKey(){
		return publickey;
	}
	
	PrivateKey getPrivateKey(){
		return privatekey;
	}

}
